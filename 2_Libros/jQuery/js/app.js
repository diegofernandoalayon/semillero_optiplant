$(document).ready(function () {
  var validar = false;
  $("#btn1").click(function () {
    $("header").addClass("hover");
    $("header").removeClass("no_hover");
    $(".lista").append("<li>" + $(".entrada").val() + "</li>");
    $(".entrada").val("");
  });
  $("#btn2").click(function () {
    $("header").addClass("no_hover");
    $("header").removeClass("hover");

    if ($(".entrada").val() !== "") {
      for (let i = 0; i <= $(".lista li").length; i++) {
        if ($(".lista li")[i].innerText == $(".entrada").val()) {
          $(".lista li")[i].remove();
          $(".entrada").val("");
        }
      }
    } else {
      $(".lista").children().last().remove();
      // $(".lista").children().first().remove();
    }
  });
  $("header").hover(
    function () {
      $(this).addClass("my_hover");
      $(this).css("color", "red");
    },
    function () {
      $(this).removeClass("my_hover");
    }
  );

  $(".cuadro").click(function () {
    // $(".cuadro").slideToggle(250);
    // $(".cuadro").slideToggle(4000);
    $(this).animate({ opacity: 0.4, left: "+=300", height: "toggle" });
    $(this).animate({ opacity: 1, left: "-=300px", height: "toggle" });
  });
  $(".cuadro2").click(function () {
    if (validar) {
      $(".cuadro2").animate(
        { marginLeft: "200px", opacity: 1, width: "200", height: "200" },
        3000
      );
      validar = false;
    } else {
      $(".cuadro2").animate(
        { marginLeft: "0", opacity: 0.4, width: "50", height: "50" },
        3000
      );
      validar = true;
    }
  });
  // $("#slide_me input").val();

  //
  //
  //$("#radio").buttonset();
  // $("#slide_me").slider({
  //   value: 0,
  //   min: 0,
  //   max: 100,
  //   step: 5,
  //   orientation: "horizontal",
  // });

  $("#radio_select").buttonset();

  $("#slide_me").slider({
    slide: function (event, ui) {
      $("#my_value").val(ui.value);
    },
  });
  // $("#my_value").val($("#slide_me").slider("value"));

  $("#ancho").slider({
    value: 40,
    min: 0,
    max: 500,
    step: 2,
    slide: function (event, ui) {
      $("#altoancho").css("width", ui.value);
    },
  });
  $("#alto").slider({
    value: 40,
    min: 0,
    max: 500,
    step: 2,
    slide: function (event, ui) {
      $("#altoancho").css("height", ui.value);
    },
  });
  // $("#red", "#green", "#blue").slider({
  //   orientation: "horizontal",
  //   // range: "min",
  //   max: 255,
  //   min:0,
  //   value: 127,
  //   slide: actulizarColor,
  //   change: actulizarColor,
  // });
  function colores() {
    var red = $("#red").slider("value");
    var green = $("#green").slider("value");
    var blue = $("#blue").slider("value");
    var rgb = "rgb(" + red + "," + green + "," + blue + ")";
    console.log(rgb);
    $("#rgb").css("background-color", rgb);

  }



  $("#red").slider({
    value: 30,
    range: "min",
    max: 255,
    step: 1,
    //  slide: colores,
    change: colores,

    slide: function (event, ui) {
      colores();
      $("#custom-red").text(ui.value);
      $("#red .ui-slider-range").css(
        "background-color",
        "rgb(" + $(this).slider("value") + ",0,0)"
      );
    },
    create: function () {
      $("#custom-red").text($(this).slider("value"));
      $("#red .ui-slider-range").css(
        "background-color",
        "rgb(" + $(this).slider("value") + ",0,0)"
      );
     
    },
  });
  $("#green").slider({
    value: 30,
    range: "min",
    max: 255,
    step: 1,
    change: colores,
    slide: function (event, ui) {
      colores();
      $("#custom-green").text(ui.value);
      $("#green .ui-slider-range").css(
        "background-color",
        "rgb( 0," + $(this).slider("value") + ",0)"
      );
    },
    create: function () {
      $("#custom-green").text($(this).slider("value"));
      $("#green .ui-slider-range").css(
        "background-color",
        "rgb( 0," + $(this).slider("value") + ",0)"
      );
    },
  });
  $("#blue").slider({
    value: 30,
    range: "min",
    max: 255,
    step: 1,
    change: colores,
    slide: function (event, ui) {
      colores();
      $("#custom-blue").text(ui.value);
      $("#blue .ui-slider-range").css(
        "background-color",
        "rgb(0,0," + $(this).slider("value") + ")"
      );
    },
    create: function () {
      $("#custom-blue").text($(this).slider("value"));
      // coloresRango();
      $("#blue .ui-slider-range").css(
        "background-color",
        "rgb(0,0," + $(this).slider("value") + ")"
      );
    },
  });

 

  // $.ajax({
  //   url: url_a_cargar,
  //   dataType: "json",
  //   data: json,
  //   success: function (json) {},
  // });
});
