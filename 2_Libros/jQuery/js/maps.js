$(document).ready(function(){ 
    var map;
    function initialize() {
      var myLatLng = new google.maps.LatLng(40.720721, -74.005966);
      var myOptions = {
        zoom: 13,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      };
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
      google.maps.event.addListener(map, "zoom_changed", function () {
        setTimeout(moveToNewYork, 3000);
      });
      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: "hello world!",
      });
      google.maps.event.addListernet(marker, "click", function () {
        map.setZoom(8);
      });
    }
    function moveToNewYork() {
      var NewYork = new google.maps.LatLng(45.526585, -122.642612);
      map.setCenter(NewYork);
    }
    initialize();

})

