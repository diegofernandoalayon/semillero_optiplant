//jshint esversion:6
const express = require("express");
const bodyParser = require("body-parser");
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html"); //el __dirname da la direccion automaticamente
});

app.post("/", function (req, res) {
  console.log(req.body.num1);

  var num1 = Number(req.body.num1);
  var num2 = Number(req.body.num2);
  var result = num1 + num2;

  res.send("El resultado es: " + result);
});
app.get("/bmicalculator", function (req, res) {
  //para adquirir de la pagina
  res.sendFile(__dirname + "/bmicalculator.html"); //el __dirname da la direccion automaticamente
});
app.post("/bmicalculator", function (req, res) {
  //metodo para enviar
  var weight = parseFloat(req.body.weight);
  var height = parseFloat(req.body.height);
  var bmi = weight / (height * height);
  if (bmi < 25) {
    res.send("tu BMI es: " + bmi + " Es normal");
  } else if (bmi >= 25 && bmi < 30) {
    res.send("tu BMI es: " + bmi + " Sobrepeso");
  } else if (bmi >= 30) {
    res.send("tu BMI es: " + bmi + " Obesidad");
  }
});
//calculadora completa
app.get("/calculadoracompleta", function (req, res) {
  res.sendFile(__dirname + "/calculadoracompleta.html");
});
app.post("/calculadoracompleta", function (req, res) {
  var expresion = req.body.datos;
  var result = null;

  if (expresion.includes("+")) {
    var suma = expresion.split("+");
    //suma.push("suma");
    result = parseFloat(suma[0]) + parseFloat(suma[1]);
    res.send("el resultado de la suma es: " + result);
  } else if (expresion.includes("-")) {
    var resta = expresion.split("-");
    result = parseFloat(resta[0]) - parseFloat(resta[1]);
    res.send("el resultado de la resta es: " + result);
  } else if (expresion.includes("*")) {
    var multiplicacion = expresion.split("*");
    result = parseFloat(multiplicacion[0]) * parseFloat(multiplicacion[1]);
    res.send("el resultado de la multiplicacion es: " + result);
  } else if (expresion.includes("/")) {
    var division = expresion.split("/");
    result = parseFloat(division[0]) / parseFloat(division[1]);
    res.send("el resultado de la division es: " + result);
  }
});

app.listen(3000, function () {
  console.log("server started on port 3000");
});
