var numeroBotones = document.querySelectorAll(".drum").length;

for (var i = 0; i < numeroBotones; i++) {
  document.querySelectorAll(".drum")[i].addEventListener("click", function () {
    //para leer el codigo de la tecla y luego convertirla de char a code, es decir da la tecla que se pulsa
    var botonPresionado = this.innerHTML;
    crearSonido(botonPresionado);
    
  });
}

window.addEventListener("keydown", function (event) {
  var teclaPulsada = String.fromCharCode((event.keyCode)+32); //se toma el valor de la tecla y se suma 32 para que sea la minuscula
  //var teclaPulsada = event.key; se toma la tecla precionada pero no discrimina de mayusculas o minusculas
  // var s = (event.code).slice(3,4).toLowerCase();  // forma de asegurar que siempre se tome el valor asi la letra sea mayuscula

  crearSonido(teclaPulsada);
});

function crearSonido(letra) {
  animarBotones(letra);
  switch (letra) {
    case "w":
      var tom1 = new Audio("sounds/tom-1.mp3");
      tom1.play();
      break;
    case "a":
      var tom2 = new Audio("sounds/tom-2.mp3");
      tom2.play();
      break;
    case "s":
      var tom3 = new Audio("sounds/tom-3.mp3");
      tom3.play();
      break;
    case "d":
      var tom4 = new Audio("sounds/tom-4.mp3");
      tom4.play();
      break;
    case "j":
      var snare = new Audio("sounds/snare.mp3");
      snare.play();
      break;
    case "k":
      var crash = new Audio("sounds/crash.mp3");
      crash.play();
      break;
    case "l":
      var kick = new Audio("sounds/kick-bass.mp3");
      kick.play();
      break;
    default:
      console.log(letra);
  }
}

function animarBotones(boton){
var botonActivo = document.querySelector("."+boton);
botonActivo.classList.add("pressed");
setTimeout(function(){
botonActivo.classList.remove("pressed");
},100);
  
}
// var audio = new Audio('sounds/crash.mp3');
// audio.play();
