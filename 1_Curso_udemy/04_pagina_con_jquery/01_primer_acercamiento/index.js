//$('h1.h13').css("color","red");  //document.querySelector('h1').style.color="red"; //seria el mismo resultado
//$(".h12").css("font-size","5rem");
//************************************* */
//manipulacion de estilos con jquery
//**************************************** */
$("h1.h11").addClass("big-title margin50"); //agremar clases
$("h1.h11").hasClass("margin50"); //preguntar si tiene una clase

//$("h1.h11").removeClass("big-title margin50"); //eliminar clases

//*************************************************** */
// manipulacion de texto con jquery
//******************************************************* */
$("h1.h13").text("pepe");
$("button").text("no pulsar"); //para modificar el texto
$("button").html("<em>hoola</em>"); //equivalente a innerHTML

//
// manipulacion de atributos con jquery
//
console.log($("img").attr("src")); //se pueden ver los atributos

$("a").attr("href", "https://www.yahoo.com"); // se pueden modificar los atributos

$("h1").attr("class");

//*********************************************************************** */
//event listers with jQuery
///******************************************************************** */

$("h1").click(function () {
  $("h1").css("color", "purple");
});

$("button").click(function () {
  //con eso se identifican todos los botones, sin tener   que hacer el for con el querySelectorAll()[i]
  $("h1").css("color", "red");
});

$(window).keypress(function (event) {
  // para el evento en toda la ventana
  //  console.log(event.key);
  $("h1").text(event.key);
});

// $("input").keypress(function(event){  //evento de teclado en el cuadro de texto
//     console.log(event.key);
// });

// $("input").on('keypress',function(event){  //funciona igual que cuando era con js solo
//     if(event.key ==="j"){
//         $("h1").css('color','coral');
//         console.log("entro aca");
//     }

// })

$("h1").on("mouseover", function (event) {
  //funciona igual que cuando era con js solo

  $("h1").css("color", "coral");
});


//********************************************************** */
/// añadiendo y eleminando elementos con jQuery
///********************************************/
$("h1").before("<button>HOLA</button>"); //antes

$("h1").after("<button>HOLA</button>");//despues

$("h1").prepend("<button>HOLA</button>"); //justo antes del texto
$("h1").append("<button>HOLA</button>"); //justo despues del texto

//******************************************************* */
//animacion con jQuery
//************************************************* */

$("h1").click(function(){
    $("h1").hide();   //para esconder cosas
});

// $("button").click(function(){
//     $("h1").show();    //para mostrar cosas

// });

// $("button").click(function(){
//     $("h1").toggle();      //selector (palanca)

// });
// $("button").click(function(){
//     $("h1").fadeOut();    //desaparece con animacion de transparencia

// });
// $("button").click(function(){
//     $("h1").fadeIn();    //para hacer aparecer cosas con animacion tranparencia

// });
// $("button").click(function(){
//     $("h1").fadeToggle();    // palanca de transparencia

// });

// $("button").click(function(){
//     $("h1").slideUp();    //ocultar en animacion de slide para arriba

// });
// $("button").click(function(){
//     $("h1").slideDown();    // aparece en animacion de slide para abajo

// });
// $("button").click(function(){
//     $("h1").slideToggle();    // palacan de slide

// });
$("button").click(function(){
    $("h1").slideUp().slideDown().animate({opacity: 0.3}).animate({margin:"20%"});    //para mostrar cosas

});