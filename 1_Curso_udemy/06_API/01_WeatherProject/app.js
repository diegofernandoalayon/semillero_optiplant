const express = require("express");
const https = require("https");
const bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.urlencoded({extended:true}));

app.get("/", function (req, res) {

  res.sendFile(__dirname+"/index.html");
  
});
app.post('/',function(req,res){
  
//////
 const query = req.body.ciudad;
  const apiKey = "224c56da71671fa399b29cea7a438121";
  const unit = "metric";
  //lat=4.52808&lon=-75.704  //Armenia Quindio
  const url =
    "https://api.openweathermap.org/data/2.5/weather?q="+query+"&appid="+apiKey+"&units="+unit+"&lang=sp"; 
  https.get(url, function (response) {
    console.log("mensaje de status: " + response.statusCode); //si retorna 200 significa correcto
    response.on("data", function (data) {
      const weatherData = JSON.parse(data); //para obtener la informacion en formato JSON
      const temp = weatherData.main.temp;
      const WeatherDescription = weatherData.weather[0].description;
      const icon = weatherData.weather[0].icon;
      const imageURL = "http://openweathermap.org/img/wn/"+icon+"@2x.png";

      
      res.write("<p>El tiempo es: " + WeatherDescription +"</p>");
      res.write("<h2> La temperatura en "+query+" es: " + temp +" grados Celsius</h2>");
      res.write("<img alt='imagen del tiempo' src="+imageURL+">");
      res.send();
      //console.log(temp);
      console.log(icon);

      // const object ={
      //     name: "Diego",
      //     comida: "Arroz"
      // }
      // console.log(JSON.stringify(object)); //para convertir un objecto a formato JSON
      //console.log(weatherData);

      //console.log(data);
    });
  });
  //res.send("server is up running");

});


app.listen(3000, function () {
  console.log("Server is running on port 3000");
});
