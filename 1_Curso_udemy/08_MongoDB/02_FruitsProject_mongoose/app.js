//jshint esversion:6
//C  -create
//R  -read
//U  -update
//D  -delete
const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017/fruitsDB", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
//
//Create
//
const fruitSchema = new mongoose.Schema({
  name: { type: String, required: [true, "debes poner un nombre"] },
  score: { type: Number, min: 1, max: 10 }, //para limitar el valor de entrada a la DB
  review: String,
});

const Fruit = mongoose.model("Fruit", fruitSchema);

const fruit = new Fruit({
  // name: "Apple",
  score: 10,
  review: "Peaches are so yummy!.",
});
//fruit.save();  //para guardar

const personSchema = new mongoose.Schema({
  //se crea la estructura asi
  name: String,
  age: Number,
  favouriteFruit: fruitSchema,
});
const Person = mongoose.model("Person", personSchema); //se define asi
const mango = new Fruit({
  name: "Mango",
  score: 6,
  review: "Decent fruit.",
});
//mango.save();

// const person = new Person({  //sirve para crear una nueva persona
//   //se le asignan elementos asi
//   name: "Amy",
//   age: 12,
//   favouriteFruit: pineapple
// });
// person.save();  //de esta manera se guarda en una base de datos
Person.updateOne({ name: "John" }, { favouriteFruit: mango }, function (err) {
  //actulizar para agregar fruta favorita
  if (err) {
    console.log(err);
  } else {
    console.log("se actulizo John correctamente");
  }
});

const kiwi = new Fruit({
  name: "Kiwi",
  score: 10,
  review: "the best fruit",
});
const orange = new Fruit({
  name: "Orange",
  score: 4,
  review: "Too sour for me",
});
const banana = new Fruit({
  name: "Banana",
  score: 3,
  review: "Weird texture",
});
// Fruit.insertMany([kiwi, orange, banana], function (err) {
//   //asi se insertan elementos en una collecion de una base de
//   if (err) {
//     console.log(err);
//   } else {
//     console.log("se guardaron correctamente las frutas en fruitsDB");
//   }
// });

///////////////////////////////////////////////////////////////////////
//Read
//////////////////////////////////////////////////////////////////////
Fruit.find(function (err, fruits) {
  if (err) {
    console.log(err);
  } else {
    // console.log(fruits);
    mongoose.connection.close(); //para finalizar la conexion
    fruits.forEach(function (fruit) {
      console.log(fruit.name);
    });
  }
});

/////////////////////////////////////////////////////////////////
//Uptade
////////////////////////////////////////////////////////////////////
// Fruit.updateOne(
//   { _id: "604abe0add90302a9cb58784" },
//   { name: "Peach" },
//   function (err) {
//     if (err) {
//       console.log(err);
//     } else {
//       console.log("se actulizo correctamente el documento.");
//     }
//   }
// );
/////////////////////////////////////////////////////////////////////////////
//Delete
////////////////////////////////////////////////////////////////////////////////
// Fruit.deleteOne({ name: "Peach" }, function (err) {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log("se ha borrado bien");
//   }
// });

// Person.deleteMany({ name: "John" }, function (err) {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log("se han borrado varios bien");
//   }
// });

///
///
//
///
///
///
///
///
///
///
//

const findDocuments = function (db, callback) {
  // Get the documents collection
  const collection = db.collection("fruits");
  // Find some documents
  collection.find({}).toArray(function (err, fruits) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(fruits);
    callback(fruits);
  });
};
