// creacion de modulos
//jshint esversion:6

exports.getDates = function () {
  //forma corta de crear
  const today = new Date();
  const options = {
    weekday: "long",
    day: "numeric",
    month: "long",
  };

  return today.toLocaleDateString("es-CO", options); //es-CO, para indicar español en cambio de en-USF
};

module.exports.getDays = getDay; //forma larga
function getDay() {
  const today = new Date();
  const options = {
    weekday: "long",
  };

  return today.toLocaleDateString("es-CO", options); //es-CO, para indicar español en cambio de en-USF
}

