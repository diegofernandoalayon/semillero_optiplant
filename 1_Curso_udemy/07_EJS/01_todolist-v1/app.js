const express = require("express");
const bodyParser = require("body-parser");
const date = require(__dirname + "/date.js"); //forma de acceder al modulo creado

// console.log(date());

const app = express();
const items = []; //pueden ser constantes porque se permite la incorporacion mediante push y no por igualacion
const itemsTrabajo = [];

app.set("view engine", "ejs"); //permite usar ejs
app.use(bodyParser.urlencoded({ extended: true })); //para habilitar el body parser
app.use(express.static("public")); //para acceder a los archivos que se asocien con la pagina

app.get("/", function (req, res) {
  const dia = date.getDates(); //asi se llama el modulo
  res.render("list", { tituloLista: dia, itemsNuevos: items }); //forma de llamar a la pagina de EJS que se crea y se le envian las variables
});
app.post("/", function (req, res) {
  let item = req.body.newItem; //guardar lo ingresado en el input
  
  if (req.body.list === "trabajos") {
    itemsTrabajo.push(item);
    res.redirect("/work");
  } else {
    items.push(item);
    res.redirect("/");
    //res.render("list",{itemNuevo:item});
  }
});
app.get("/work", function (req, res) {
  res.render("list", { tituloLista: "trabajos", itemsNuevos: itemsTrabajo });
});
app.get("/about", function (req, res) {
  res.render("about");
});

app.post("/work", function (req, res) {
  let item = req.body.newItem;
  itemsNuevos.push(item);
  res.redirect("/work");
});

app.listen(3000, function () {
  console.log("server running");
});
