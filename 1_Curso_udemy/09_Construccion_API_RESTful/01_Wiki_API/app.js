//jshint esversion:6

const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const mongoose = require("mongoose");

const app = express();

app.set("view engine", "ejs");

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(express.static("public"));

mongoose.connect("mongodb://localhost:27017/wikiDB", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const articleShema = {
  title: String,
  content: String,
};
const Article = mongoose.model("Article", articleShema);
/////////////////////////////////////para todos los articulos////////////////
app
  .route("/articles")
  .get(function (req, res) {
    //get
    Article.find(function (err, foundArticles) {
      if (!err) {
        res.send(foundArticles);
      } else {
        res.send(err);
      }
    });
  })
  .post(function (req, res) {
    //post
    //   console.log(req.body.title);
    //   console.log(req.body.content);
    const newArticle = new Article({
      title: req.body.title,
      content: req.body.content,
    });
    newArticle.save(function (err) {
      if (!err) {
        res.send("Articulo añadido correctamente");
      } else {
        res.send(err);
      }
    });
  })
  .delete(function (req, res) {
    //delete
    Article.deleteMany(function (err) {
      if (!err) {
        res.send("Se borraron los articulos correctamente");
      } else {
        res.send(err);
      }
    });
  });
////////////////////////////////////para articulos especificos/////////////

app
  .route("/articles/:tituloArticulo")
  .get(function (req, res) {
    Article.findOne(
      { title: req.params.tituloArticulo },
      function (err, foundArticle) {
        if (foundArticle) {
          res.send(foundArticle);
        } else {
          res.send("no se encontro el articulo!");
        }
      }
    );
  })
  .put(function (req, res) {
    Article.update(
      { title: req.params.tituloArticulo },
      { title: req.body.title, content: req.body.content },
      { overwrite: true },
      function (err) {
        if (!err) {
          res.send("Se actulizo el articulo");
        } else {
          res.send("ocurrio un error en actulizar");
        }
      }
    );
  })
  .patch(function (req, res) {
    Article.update(
      { title: req.params.tituloArticulo },
      { $set: req.body },
      function (err) {
        if (!err) {
          res.send("articulo actulizado correctamente");
        } else {
          res.send(err);
        }
      }
    );
  })
  .delete(function (req, res) {
    Article.deleteOne({ title: req.params.tituloArticulo }, function (err) {
      if (!err) {
        res.send("se borro el articulo: " + res.params.tituloArticulo);
      } else {
        res.send("no se pudo borrar el articulo");
      }
    });
  });

app.listen(3000, function () {
  console.log("Server started on port 3000");
});
