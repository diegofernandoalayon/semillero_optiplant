let persona = "pepe";
let edad = 28;
function myfunc(string, expPersona, expEdad) {
  let str0 = string[0];
  let str1 = string[1];
  let strEdad;
  if (expEdad === 28) {
    strEdad = "gokusito";
  }
 //se retorna una cadena usando literal
  return `${str0}${expPersona}${str1}${strEdad}`;
}
//se llama la funcion con el uso de literal
var salida = myfunc`Ese ${persona} es un ${edad}`;
console.log(`Esta es una salida:
 ${salida} 
  con el uso de literales 
   es posible hacer textos 
  de varias lineas y multiples 
 espacios de manera sencilla 
como este`);

