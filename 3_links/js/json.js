$(document).ready(function () {
  var objetoJSON;
  objetoJSON = { "nombre": "Diego", "edad": 22 }; // asi se define un objeto JSON
  var jsonCadena = JSON.stringify(objetoJSON); // asi se convierte el objeto JSON a una cadena
  var jsonDeNuevo = JSON.parse(jsonCadena); // asi de convierte el objeto JSONcadena nuevamente a un objeto JSON, es la accion contraria a el paso anterior
    $("#contenido").html(objetoJSON.nombre); //mostramos dentro de contenido el objetoJSON y su atributo nombre
  // $("#contenido").html(jsonCadena);
  var objetos = {
    //objeto JSON con una arreglo de objetos JSON
    
    "personas": [
      { "nombre": "Diego", "edad": 22 },
      { "nombre": "pepe", "edad": 34 },
      { "nombre": "jose", "edad":32 },
      { "nombre":"maria", "edad":19 },
    ],
  };
  $("#contenido").append(`<h3>${objetos.personas[0].nombre}</h3>`);// 
  $("#contenido").append(`<h3>${objetos.personas[0].edad}</h3>`);
  objetos.personas.forEach(function(persona){
      $("#nombres").append(`<option value="${persona.nombre}">${persona.nombre}</option>`);
  });
  //sacando la informacion del objeto JSON y creando un seleccionable
  for(var i=0;i<objetos.personas.length;i++){
      $("#edades").append(`<option value="${objetos.personas[i].edad}">${objetos.personas[i].edad}</option>`)
  }
});
// definicion de un objeto en php y codificacion a objeto JSON
// <?php
// $myObj->name = "John";
// $myObj->age = 30;
// $myObj->city = "New York";

// $myJSON = json_encode($myObj);

// echo $myJSON;
// ?>
