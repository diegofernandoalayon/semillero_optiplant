//Parametros
// async : Valor booleano que indica si la solicitud debe ser asicrono o no, por defecto es true

//beforeSend(xhr) : una funcion que se puede ejecutar antes de que se envie la solicitud

//cache : valor booleano que indica si el navegador debe almacenar en cache las paginas solicitadas, por defecto es true

//complete(xhr,status) : funcion que se puede ejecutar despues de que finalice la solicitud

//contentType : el tipo de contenido utilizado al enviar datos al servidor. El valor predeterminado es: "application/x-www-form-urlencoded"

//context : Especifica el valor "this" para todas las funciones de callback relacionadas con AJAX

//data : especifica los datos que se enviaran al servidor

// dataFilter(data,type): Una funcion utilizada para manejar los datos de prespues sin procesar de XMLHttpRequest

// dataType: indica el tipo de datos esperado de la respuesta del servidor

//error(xhr,status,error): Funcion que se puede ejecutar si la solicitud falla

// global : Un valor booleano que especifica si se activan o no los controladores de eventos AJAX globales para la solicitud. El valor predeterminado es verdadero.

//jsonp : una cadena que anula la funcion de callback en una solicitud jsonp

//jsonpCallback : especifica un nombre para la funcion de callback en una solicitud jsonp

//password : especifica una contrase;a que se utilizara en una solicitud de autenticacion de acceso HTTP

//processData : un valor booleano que especifica si los datos enviados con la solicitud deben tranformarse en una cadena de consulta. El valo por defecto es true

//scripCharset : especifica el juego de caracteres para la solicitud

//succes(result,statud,xhr) : una funcion que se puede ejecutar cuando la solicitud funcione correctamente.

//timeout : tiempo de espera local para la solicitud, dada en milisegundos.

//traditional : un valor booleano que especifica si se debe utilizar o no el estilo tradicional de serializacion de parametros

//type : especifica el tipo de solicitud (GET o POST)

// url : especifica la URL a la que se envia la solicitud. El valor por defecto es la pagina actual.

//username : especifica un nombre de usuario que se utilizara en una solicitud de autenticacion de acceso HTTP

//xhr : una funcion utilizada para crear el objeto XMLHttpRequest.

$(document).ready(function () {
    $("#boton").click(function () {
      $.ajax({
        url: "js/test.js",
        dataType: "script",
        type: "POST",
        //   async: false,
        //   data: "pepe",
        success: function (result) {
          console.log(result);
        },
        error: function () {
          alert("fallo");
        },
        timeout:100,  //tiempo maximo que puede tardar la peticion
      });
    //   return false; // hace que la pagina no se recarge al enviar el formulario
    });
  
  $("#boton1").click(function () {
    alert("boton1");
    $.ajax({
      url: "app.php",
      type: "POST",
      success: function (result) {
        alert("funciono");
        console.log(result);
        alert('jaksdj');
      },
      error: function (xhr, status, error) {
        console.log(xhr);
      },
    });
    return false; // hace que la pagina no se recarge al enviar el formulario
  });
  $("#formulario").submit(function(){ //funciona de igual manera que el return false despues de usar AJAX
      return false;
  })

});
