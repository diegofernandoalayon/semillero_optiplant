$(document).ready(function () {
  $("#btn").click(function () {
    $("#division").data("greeting", "Hello World");
    $("h1").css("color", "blue");
  });
  $("#btn1").click(function () {
    // alert($("#division").data("greeting"));
    $("#segundo").css("background-color", "blue");
    alert($("#division").data("rs")); //get
  });
  $("#btn2").on("click", function () {
    $("#division").data("rs", "otros datos"); //set
  });

  // filtrar seleccion y encontrar todas las etiquetas que tengas el data-rs
  var datos = $("*").filter(function () {
    return $(this).data("rs");
  });
  console.log(datos);
  // filtrar y encontra solo la etiqueta que tenga el data con el valor deseado
  var datosMejor = $("*").filter(function () {
    return $(this).data("rs") == "division de select";
  });
  console.log(datosMejor);
});
